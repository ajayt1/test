


import { runtimeEnvScript } from "vite-runtime-env-script-plugin";

export default {
  plugins: [runtimeEnvScript({ variables: ["CLOUD_URL","EDGE_URL","BACKEND_URL","PRICYTRAY_URL"] })],
};
