import React, { createContext,useEffect,useState } from "react";


export const mainContext = createContext({});
const Store = ({ children }) => {
  let userData = sessionStorage.getItem("userData");
  let spinnerData = sessionStorage.getItem("spinner");
  let anonymousUserData = sessionStorage.getItem("anonymousUserData");
  let anonymousUserID = sessionStorage.getItem("anonymousUserId");
  let cameraMode = sessionStorage.getItem("cameraMode");
  let cameraUrl = sessionStorage.getItem("cameraUrl");
  const [userInfo, setUserInfoS] = useState(userData!=null?JSON.parse(userData):{});
  const [spinnerInfo, setSpinnerInfoS] = useState(spinnerData!=null?spinnerData:"");
  const [anonymousUserInfo, setAnonymousUserInfos] = useState(anonymousUserData!=null?anonymousUserData:"");
  const [anonymousUserId, setAnonymousUserIds] = useState(anonymousUserID!=null?anonymousUserID:"");
  const [userImage,setUserImage] = useState(false);
  const [envPlatform,setEnvPlatform] = useState();
  const [camMode,setCamModeS] = useState(cameraMode!=null?cameraMode:"");
  const [camUrl,setCamUrlS] = useState(cameraUrl!=null?cameraUrl:"");
  const [galleryUserName, setGalleryUserName] = useState("");
  const [galleryUserEmail, setGalleryUserEmail] = useState("");

  useEffect(()=>{
    
  }, [])
  function setUserInfo(data){
    // console.log(data, 'data')
    const jsonData = JSON.stringify(data)
    sessionStorage.setItem('userData',jsonData);
    setUserInfoS(data);
  }
  function setSpinnerInfo(data){
    // const jsonData = JSON.stringify(data)
    sessionStorage.setItem('spinner',data);
    setSpinnerInfoS(data)
  }
  function setAnonymousUserInfo(data){
    // const jsonData = JSON.stringify(data)
    sessionStorage.setItem('anonymousUserData',data);
    setAnonymousUserInfos(data)
  }
  function setAnonymousUserId(data){
    // const jsonData = JSON.stringify(data)
    sessionStorage.setItem('anonymousUserId',data);
    setAnonymousUserIds(data)
  }
  function setCameraMode(data){
    // const jsonData = JSON.stringify(data)
    sessionStorage.setItem('cameraMode',data);
    setCamModeS(data)
  }
  function setCamUrl(data){
    // console.log(data, 'data')
    // const jsonData = JSON.stringify(data)
    sessionStorage.setItem('cameraUrl',data);
    setCamUrlS(data);
  }


  return (
    <div>
      <mainContext.Provider value={{ 
        userInfo, 
        setUserInfo,
        spinnerInfo,
        setSpinnerInfo,
        anonymousUserInfo,
        setAnonymousUserInfo,
        anonymousUserId,
        setAnonymousUserId,
        userImage,
        setUserImage,
        envPlatform,
        setEnvPlatform,
        camMode,
        setCameraMode,
        camUrl,
        setCamUrl,
        setGalleryUserName,
        galleryUserName,
        setGalleryUserEmail,
        galleryUserEmail
      }}>
        {children}
      </mainContext.Provider>
    </div>
  );
};

export default Store;
