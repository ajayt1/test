import React, { useEffect, useState, useRef } from "react";
import { Carousel } from "react-responsive-carousel";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

import "./eventphoto.css";
// import ImageSlider from "../ImageCarousel/ImageCarousel";
import { getApi, getApiUrl2, postApiUrl2 } from "../../service/service/Service";
import { apiPaths } from "../../service/apipaths/ApiPaths";
import { url } from "../../service/url/url";
import { mainContext } from "../../store/Store";
import Logo from "../../assets/Logo.png";
import settingIcon from '../../assets/btn-setting.png';
import printerIcon from '../../assets/printer_icon.png';
import priceTrayIcon from '../../assets/thumb-TT-selected1.png';
import poseOffIcon from '../../assets/thumb-TT-selected.png';
import homepageBanner from '../../assets/updatedPose/Sequence1.mp4';

const EventPhotoPage = () => {
  const navigate = useNavigate();
  const { setAnonymousUserInfo, setAnonymousUserId, envPlatform, camMode, camUrl, spinnerInfo,setEnvPlatform, setCameraMode, setCamUrl } = useContext(mainContext);
  const [carouselImages, setCarouselIMages] = useState([]);
  const [isPlaying, setIsPlaying] = useState(true);
  const pricyURL = process.env.VITE_PRICYTRAY_URL;

  const getPhotos = async () => {
    const response = await getApiUrl2(apiPaths?.showPicture);
    const images = response && response?.data?.map(
      (item) => `${url?.url2}/${item.img_link}`
    );
    setCarouselIMages(images);
  };

  const startGame = (e) => {

    if (e == 'poseoff') {
      postApiUrl2({}, apiPaths.getPlayerName, "application/json")
        .then((res) => {
          setAnonymousUserId(res.data[0]._id);
          setAnonymousUserInfo(res.data[0].username);
        })
        .catch((err) => {
        })
      if (envPlatform == 'EDGE') {
        navigate('/SelectedService');
        return;
      }
      else {
        navigate('/PlatformSelectPage');
      }
    }
    else {
      window.location.href = pricyURL;
    }
  }

  const videoRef = useRef(null);

  const handleVideoEnd = () => {
    videoRef.current.currentTime = 0;
    videoRef.current.play();
    if (carouselImages && carouselImages.length > 0) {
      setIsPlaying(false);
    }

  };

  const handleGallery = () => {
    navigate('/PrintingPage');
  }

  const handleSetting = () => {
    navigate('/Settings');
  }

  const handleSaveSetting = () => {
    // setUpdating(1);

    const sendData = {
      "game_mode": envPlatform ? envPlatform : "",
      "cam_mode": camMode ? camMode : "",
      "cam_url": camUrl ? camUrl : ""
    }
    postApiUrl2(sendData, apiPaths.changeSettings, "application/json")
      .then((res) => {
        setEnvPlatform(res.data.game_mode);
        setSpinnerInfo(res.data.game_mode);
        setCameraMode(res.data.cam_mode);
        setCamUrl(res.data.cam_url);
        setTimeout(() => {
          setUpdating(2);
          setTimeout(() => {
            navigate('/EventPhoto');
          }, 1000)
        }, 1000)
      })
      .catch((err) => {
        // setUpdating(0);
      })
  }
  const getSettings = async () => {
    const response = await getApiUrl2(apiPaths?.getSettings);
    const data = response?.data[0];
    if (data.game_mode != "") {
      setEnvPlatform(data.game_mode)
    }
    if (data.cam_mode != "") {
      setCameraMode(data.cam_mode)
    }
    if (data.cam_url != "") {
      setCamUrl(data.cam_url)
    }
    
  }

  useEffect(() => {
    console.log("url :",process.env.VITE_CLOUD_URL);
    getSettings();
    getPhotos();
    handleSaveSetting();
  }, []);

  return (
    <div className="parentConatiner">
      <div className="event_photo_header">
        <div className="logoImgEvent">
          <img id="glogoImageEvent" src={Logo}></img>
        </div>
        <div className="header_icon_box">
          <div onClick={handleSetting}><img src={settingIcon} /></div>
          <div onClick={handleGallery}><img src={printerIcon} /></div>
        </div>
      </div>
      <div className="carouselContainer">
        <div>
          <div className="games_heading">Games To Play</div>
          <div>
            <div className="game_container">
              <div className="img_game_img" onClick={(e) => startGame('poseoff')}><img src={poseOffIcon} /></div>
              <div className="poseoff_text">
                <div className="p_text">Pose Off</div>
                <div className="instruction_text">Strike exciting poses and outmaneuver your opponents</div>
              </div>
            </div>
            <div className="game_container">
              <div className="img_game_img" onClick={(e) => startGame('pricytray')}><img src={priceTrayIcon} /></div>
              <div className="poseoff_text">
                <div className="p_text">Price a Tray</div>
                <div className="instruction_text">Make a tray of target price by placing combo of objects in tray</div>
              </div>
            </div>
          </div>
        </div>
        <div className="galleryImg">
          {isPlaying == true ?
            <div className="carouselVid">
              <video
                ref={videoRef}
                autoPlay
                muted
                src={homepageBanner}
                width="600"
                height="400"
                controlsList="nodownload"
                onEnded={handleVideoEnd}
              />
            </div> :
            <>
              {carouselImages && carouselImages.length > 0 &&
                <Carousel
                  autoPlay={true}
                  infiniteLoop
                  interval={3000}
                  showArrows={false}
                  showIndicators={false}
                  showThumbs={false}
                >
                  {carouselImages?.map((item) => (
                    <div key={item}>
                      <img className="carouselImg" src={item} />
                    </div>
                  ))}
                </Carousel>
              }
            </>
          }
        </div>
      </div>
      <div className="bottomFooterEvent">
        <p className="bottomMarkTextEvent">
          Powered by <b>Google</b>
        </p>
      </div>
    </div>
  );
};

export default EventPhotoPage;
