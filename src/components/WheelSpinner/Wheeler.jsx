import React, { useEffect, useState, useRef } from "react";
import stand from '../../assets/Stand.png';
import frame from '../../assets/frame.png';
export default function Wheeler({colors, texts, parts, onFinish}){

  const cvs = useRef(null);
  const ctx = useRef(null);
  const angle = useRef(null);
  const center = useRef({x:200, y:200}).current;
  const radius = 180;
  const standImg = useRef(null);
  const frameImg = useRef(null);
  const hasSpin = useRef(false);
  // const parts = useRef(8).current;
  const rotateAngle = useRef(0);
  const speed = useRef(0);
  // const colors = colors;
  // const text = texts
  let currentPart = "";
  // useEffect(()=>{
  //   setTimeout(()=>{
  //     spin()
  //   },5000)
  // })
  useEffect(()=>{
    cvs.current = document.getElementById("wheeler_canvas");
    if(cvs.current==null){
      return;
    }

    standImg.current = new Image();
    standImg.current.src = stand;
    frameImg.current = new Image();
    frameImg.current.src = frame;
    let imgLoaded = 0;
    standImg.current.onload = ()=>{
      imgLoaded++;
      if(imgLoaded==2){
        draw();
      }
    }
    frameImg.current.onload = ()=>{
      imgLoaded++;
      if(imgLoaded==2){
        draw();
      }
    }
    ctx.current = cvs.current.getContext("2d");
    angle.current = 2*Math.PI/parts;
    function canvasClick(event) {
      let cvsSize = cvs.current.getBoundingClientRect();
      let ratioX = cvs.current.width/cvsSize.width;
      let ratioY = cvs.current.height/cvsSize.height;
      let x = (event.clientX-cvsSize.left)*ratioX;
      let y = (event.clientY-cvsSize.top)*ratioY;
      if(dist(x, y, center.x, center.y)<180){
        spin();
      }
      
    }
    cvs.current.addEventListener("click", canvasClick)

    setTimeout(()=>{
      if(!hasSpin.current){
        spin()
      }
    },5000)
    // draw();
    return ()=>{

    }
  }, []);

  function dist(x1, y1, x2, y2){
    return Math.sqrt(Math.pow((x2-x1), 2)+ Math.pow((y2-y1), 2));
  } 
  function draw(){
    if(ctx.current==null)return;


    ctx.current.clearRect(0, 0, cvs.current.width, cvs.current.height)
    ctx.current.save();
    let height = cvs.current.width*573/1610;
    ctx.current.drawImage(standImg.current, 0, cvs.current.height-height+10, cvs.current.width, height);
    drawCircle(center.x, center.y, radius, 'rgba(0,0,0,0.2)');
    // drawCircle(center.x, center.y, radius-15, '#ffffff');
    
    let r = radius-15;
    
    for(let i=0; i<parts; i++){
      let partAng = angle.current*i + rotateAngle.current;
      let x = r*Math.cos(partAng);
      let y = r*Math.sin(partAng);
      drawSagment(x, y, colors[i], partAng, r, texts[i], i);
      // let smallAng = partAng+angle.current/2;
      // let cX = center.x+(r+8)*Math.cos(smallAng);
      // let cY = center.y+(r+8)*Math.sin(smallAng);
      // drawCircle(cX, cY, 5, "orange")
    }
    // drawCircle(center.x, center.y, 20, "#9e7005")
    ctx.current.drawImage(frameImg.current, 16, 14, cvs.current.width-32, cvs.current.width-30);

    // drawArrow("#9e7005")
    ctx.current.restore();
  }
  function drawCircle(x, y, radius, color=null){
    ctx.current.save();
    ctx.current.beginPath();
    ctx.current.arc(x, y, radius, 0, 2 * Math.PI);
    
    if(color!=null){
      ctx.current.fillStyle = color;
      ctx.current.strokeStyle = color;
      ctx.current.fill();
    }
    ctx.current.stroke();
    ctx.current.closePath();
    // drawCircle(center.x, center.y, radius, 'rgba(0,0,0,0.2)');
    ctx.current.restore();
  }
  function drawSagment(x, y, color, partAng, radius, text, num){

    ctx.current.save();
    ctx.current.translate(center.x, center.y);
    ctx.current.fillStyle = color;
    ctx.current.beginPath();
    ctx.current.moveTo(0, 0);
    ctx.current.lineTo(x, y)
    ctx.current.arc(0, 0, radius, partAng, partAng+angle.current);
    ctx.current.lineTo(0, 0);
    
    ctx.current.fill();
    ctx.current.closePath();
    const textX = radius/2*Math.cos(partAng+angle.current/2);
    const textY = radius/2*Math.sin(partAng+angle.current/2);
    ctx.current.translate(textX, textY)
    ctx.current.rotate(partAng+angle.current/2)
    ctx.current.fillStyle = "#ffffff"
    ctx.current.font = "bold 18px Arial"
    ctx.current.fillText(text, 0, 0)
    ctx.current.restore();
    if(x<0 && radius*Math.cos(partAng+angle.current)>0){
      currentPart = num;
    }
  }
  function drawArrow(color="#000000"){
    const x = center.x, y=20, size=20;
    ctx.current.save();
    ctx.current.fillStyle = color;
    ctx.current.beginPath();
    ctx.current.moveTo(x-size, y);
    ctx.current.lineTo(x+size, y);
    ctx.current.lineTo(x, y+size)
    ctx.current.lineTo(x-size, y);
    ctx.current.closePath();
    ctx.current.fill();
    ctx.current.restore();
  }
  function spin() {
      if(speed.current!=0){
        return;
      }
      if(!hasSpin.current){
        hasSpin.current = true;
      }

      setTimeout(()=>{
        rotateAngle.current = Math.random()*2*Math.PI;
      }, 100)
      speed.current = 0.7;
      let interval = setInterval(()=>{
      rotateAngle.current = (rotateAngle.current+speed.current);
      if(rotateAngle.current>=2*Math.PI){
        rotateAngle.current = rotateAngle.current-2*Math.PI;
      }
      draw();
     
      if(speed.current<=0.1){
        const currentAng = ((parts-currentPart)*angle.current + 3*Math.PI/2-angle.current/2)%(2*Math.PI);
        const diff = Math.abs(rotateAngle.current-currentAng);
        if(diff>0.1){
          if(speed.current>=0.05){
            speed.current -= 0.008;
          }
        } else {
          speed.current = 0;
          clearInterval(interval);
          onFinish(texts[currentPart])
        }
      } else {
        speed.current -= 0.008;
      }
    }, [18]) 
  }

  function test(){
    // rotateAngle.current += speed.current;;
    draw();
    // speed.current -= 0.01;
  }
  function setRoateAngle(e){
    let val = Number(e.target.value);
    rotateAngle.current = val;
  }
  return(
      <canvas id="wheeler_canvas" width="400" height="420">

      </canvas>
  )
}