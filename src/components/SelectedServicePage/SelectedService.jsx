import React, { useEffect, useState, useRef, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { mainContext } from "../../store/Store";
import googleCloud from "../../assets/google-cloud.svg";
import ServiceHeader from "../../common/header/ServiceHeader";
import ProgressLoader from "../../common/ProgressLoader";
const SelectedService = () => {
  const navigate = useNavigate();
  const { spinnerInfo, setUserImage, envPlatform } = useContext(mainContext);
  const [isChecked, setIsChecked] = useState(false);

  const handleCheckboxChange = (event) => {
    setIsChecked(event.target.checked);
    setUserImage(event.target.checked);
  };

  const handleClose = () => {
    navigate("/EventPhoto");
  }

  const PosePage = () => {
    navigate('/Pose');
  }


  return (
    <>
      <ServiceHeader handleClose={handleClose} />
      <div className="cloud-wrapper">
        <div className="instructionList">
          <p className="instructionHeading">How to play?</p>
          <ul>
            <li>Stand in front of game screen</li>
            <li>Act the pose displayed on screen</li>
            <li>Make sure camera frame is covering your posture</li>
            <li>Skip the pose you dont want to act</li>
            <li>Game will be over when all poses are done OR time is over</li>
          </ul>
          <div className="acknowledgeCheckbox">
            <input
              type="checkbox"
              checked={isChecked}
              onChange={handleCheckboxChange}>
            </input>
            <p className="ackknowledgeTxt">I understand that my pictures will be taken while playing game.</p>
          </div>
        </div>
        <div className="right_content">
          <div className="right_top_content">
            <div className="cloud-wrapper-img"><img src={googleCloud}></img></div>
            <div className="wrapper-textHdr12">You are going to play this game on Google <span >{spinnerInfo? spinnerInfo : "EDGE"}</span></div>
          </div>
          <div className="leader__board-button-containerSS" onClick={PosePage}>
            <div className="">
              <div>LET'S PLAY</div>
              <div className="loader_boxSS">
                <ProgressLoader time={10} onFinish={PosePage} bgColor='#A0A4D7' fgColor="#CCCDD2" />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="bottomFooter">
        <p id="bottomMarkText">
          Powered by <b>Google</b>
        </p>
      </div>
    </>
  );
};

export default SelectedService;
