import * as React from 'react';
import { useState, useEffect } from 'react';
import { render } from 'react-dom';

import './digitalTimer.css';

function DigitalTimer() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCount(prevCount => prevCount + 1);
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <div className="timerParent">
      <div className="timerChild">0:{count}</div>
    </div>
  )
}

export default DigitalTimer