import React from 'react';
import "./App.css";
import {  Route, Routes } from "react-router-dom";
import LandingPage from './components/LandingPage/LandingPage';
import EventPhotoPage from './components/EventPhotoPage/EventPhotoPage';
import PlatformSelectPage from './components/PlatformSelectPage/PlatformSelectPage';
import SelectedService from './components/SelectedServicePage/SelectedService';
import LeaderBoardRanking from './components/Leaderboard/LeaderBoardRanking';
import LeaderBoard from './components/leaderBoardPrev/LeaderBoard';
import Pose from './components/pose/Pose';
import GalleryPage from './components/Gallery/GalleryPage';
import Setting from './components/Settings/Setting';
import PrintingPage from './components/PrintingPage/PrintingPage';
import {url} from './service/url/url';


function App() {

  console.log("ajay",url)
  return (
    <Routes>
      <Route path="/" element={<LandingPage />}></Route>
      <Route
        path="/EventPhoto"
        element={<EventPhotoPage />}
      ></Route>
      <Route
        path="/GalleryPage"
        element={<GalleryPage />}
      ></Route>
      <Route
        path="/PlatformSelectPage"
        element={<PlatformSelectPage />}
      ></Route>
      <Route
        path="/SelectedService"
        element={<SelectedService />}
      ></Route>
      <Route path="/Pose" element={<Pose />} />
      <Route path="/LeaderBoardRanking" element={<LeaderBoardRanking />} />
      <Route
        path="/LeaderBoard"
        element={<LeaderBoard />}
      ></Route>
      <Route
        path="/Settings"
        element={<Setting />}
      ></Route>
      <Route
        path="/PrintingPage"
        element={<PrintingPage/>}
      ></Route>
    </Routes>
  )
}

export default App
