import axios from "axios";
import { url } from "../url/url";
 
export const getApi = async (paths) => {
  //   const apiUrl = `${url.url}/${apiPaths?.search}`;
  const apiUrl = `${url?.gcpUrl}/${paths}`;
 
  try {
    const response = await axios.get(apiUrl);
    return response;
  } catch (error) {
    if (error.response && error.response.status === 500) {
      console.log("Internal Server Error");
    } else {
      console.log("An error occurred:", error.message);
    }
  }
};
 
export const postApi = async (data, path) => {
  const apiUrl = `${url.url}/${path}`;
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded",
  };
  try {
    const response = await axios.post(apiUrl, data, {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};
 
export const cloudpostApi = async (data, path) => {
  const apiUrl = `${url.cloudUrl}/${path}`;
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded",
  };
  try {
    const response = await axios.post(apiUrl, data, {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};
 
export const postApiGcp = async (data, path) => {
  const apiUrl = `${url?.gcpUrl}/${path}`;
  try {
    const response = await axios.post(apiUrl, data);
    return response;
  } catch (error) {
    return error;
  }
};

export const getApiUrl2 = async (paths) => {
  //   const apiUrl = `${url.url}/${apiPaths?.search}`;
  
  const apiUrl = `${url.url2}/${paths}`;
 
  try {
    const response = await axios.get(apiUrl);
    return response;
  } catch (error) {
    if (error.response && error.response.status === 500) {
      console.log("Internal Server Error");
    } else {
      console.log("An error occurred:", error.message);
    }
  }
};

export const getApiUrl3 = async (paths) => {
  //   const apiUrl = `${url.url}/${apiPaths?.search}`;
  const apiUrl = `${url?.url}/${paths}`;
 
  try {
    const response = await axios.get(apiUrl);
    return response;
  } catch (error) {
    if (error.response && error.response.status === 500) {
      console.log("Internal Server Error");
    } else {
      console.log("An error occurred:", error.message);
    }
  }
};

 
export const postApiUrl2 = async (data, path, contentType="application/x-www-form-urlencoded") => {
  
  const apiUrl = `${url.url2}/${path}`;
  const headers = {
    "Content-Type": contentType,
  };
  try {
    const response = await axios.post(apiUrl, data, {
      headers: headers,
    });
    return response;
  } catch (error) {
    return error;
  }
};
 
