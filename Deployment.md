# GITLAB Repos:
```
https://gitlab.com/next8741410/poseoff_backend
https://gitlab.com/next8741410/poseoff_inference
https://gitlab.com/next8741410/poseoff_frontend
```

# Kubernetes deployment:
## Note: Default sql password can be changed inside “kube_poseoff_backend/mysql.yaml’ (before next command)
## poseoff_inference
```
kubectl apply -f kube_poseoff_backend/mysql.yaml  -n poseoff-inference
```
## Note: if you change password in “mysql.yaml” above than you need to update DB_URL as well with new password in configmapbackedn.yaml   (before next command)
## poseoff_backend
```
kubectl apply -f kube_poseoff_backend/configmapbackedn.yaml  -n poseoff-inference
```
```
kubectl apply -f kube_poseoff_backend/testbackend.yaml -n poseoff-inference
kubectl expose pods poseoff-backend-deployment-5cf9d9bd4b-5hq4v --type LoadBalancer  --name backend --port 8082 -n poseoff-inference
```
```
kubectl apply -f poseoff_inference/kube_inference.yaml -n poseoff-inference
kubectl expose pods poseoff-deploy-deployment-6d9789484d-mt8dc  --type LoadBalancer  --name infrence --port 9002 -n poseoff-inference
```
## Note: before running next command, please update below URLs in “frontconfig.yaml”
    • VITE_CLOUD_URLE: "https://poseoff-edge-4yerohykja-uc.a.run.app" (Poseoff cloud deployment URL)
    • VITE_EDGE_URL: “http://34.122.205.27:9002” (Poseoff Edge deployment URL)
    • VITE_BACKEND_URL: “http://34.170.172.220:8082” (Poseoff_backend deployment URL)
    • VITE_PRICYTRAY_URL: "https://example.com" (Price a tray deployment URL)
## poseoff_frontend
```
kubectl apply -f kube_poseoff_frontend/frontconfig.yaml -n poseoff-inference
```
```
kubectl apply -f kube_poseoff_frontend/poseoffrontend.yaml -n poseoff-inference
kubectl expose pods poseoff-frontend-deployment-76cc9c46d-pw22x  --type LoadBalancer  --name frontend --port 80 -n poseoff-inference
```


#How to run 

1. Select the game mode and configure camera:
    On the home screen click on the setting icon to select game mode and configure active camera eg: webcam, usbcam, rtsp-cam (for rtsp-cam provide rtsp camera url in text box) than click on save setting. Please note if the RTSP url is incorrect user will face black screen in the game and will fail all poses.
2. Start the game
    On the home screen click on Pose game to start