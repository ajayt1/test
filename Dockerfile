FROM node:18.16-buster-slim as build

WORKDIR /app

COPY package*.json ./

RUN npm install --legacy-peer-deps

COPY . .

RUN npm run build
FROM nginx:1.17-alpine

RUN rm -f /etc/nginx/conf.d/default.conf

# COPY ./nginx.conf /etc/nginx/conf.d/nginx.conf
COPY --from=build /app/nginx.conf /etc/nginx/conf.d/nginx.conf
COPY --from=build /app/dist /usr/share/nginx/html
CMD ["/bin/sh", "-c", "envsubst < /usr/share/nginx/html/template-runtime-env.js > /usr/share/nginx/html/runtime-env.js && nginx -g \"daemon off;\""]
